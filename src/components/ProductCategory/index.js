import map from 'assets/img/map.jpg';
import CardCategory from 'components/CardCategory';
import { useFetch } from 'hooks';

const ProductCategory = () => {

    const { data, isLoading } = useFetch(
        'https://api-tdp-2022.vercel.app/api/categories'
    )
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container padding-32" id="projects">
                <h3 className="border-bottom border-light-grey padding-16">Products Category</h3>
            </div>
            {isLoading ?
                <div className="container padding-32">
                    Loading...
                </div> :
                <div className="row-padding">
                    {
                        data?.data ? data?.data?.map((obj, i) => {
                            return (
                                <CardCategory key={i} id={obj.id} name={obj.name} image={obj.image} />
                            )
                        }) :
                            <div className="container padding-32">Category Not Found</div>
                    }
                </div>
            }
            <div className="container padding-32">
                <img src={map} className="image" alt="maps" style={{ width: '100%' }} />
            </div>
        </div>
    )
}

export default ProductCategory