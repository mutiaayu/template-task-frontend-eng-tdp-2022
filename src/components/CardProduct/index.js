import React from 'react'

const CardProduct = ({id, discount, image, category, title, description, price}) => {
    return (
        <div className="product-card" key={id}>
            {discount > 0 &&
                <div className="badge">Discount</div>
            }
            <div className="product-tumb">
                <img src={image} alt={title} />
            </div>
            <div className="product-details">
                <span className="product-catagory">{category}</span>
                <h4>
                    <a href={`/shop/${id}`}>{title}</a>
                </h4>
                <p>{description}</p>
                <div className="product-bottom-details">
                    <div className="product-price">Rp. {parseInt(price).toLocaleString('id')}</div>
                    <div className="product-links">
                        <a href={`/shop/${id}`}>View Detail<i className="fa fa-heart"></i></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CardProduct