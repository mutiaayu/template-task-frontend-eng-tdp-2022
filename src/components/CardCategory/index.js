const CardCategory = ({id, name, image}) => {
    return (
        <div className="col l3 m6 margin-bottom" key={id}>
            <a href={`/shop?category=${id}`}>
                <div className="display-container" style={{ boxShadow: '0 2px 7px #dfdfdf', display: 'flex', alignItems: 'center', justifyContent: 'center', height: '300px', padding: '80px' }}>
                    <div className="display-topleft black padding">{name}</div>
                    <img src={image} alt="House" style={{ maxWidth: '100%', minHeight: '100%' }} />
                </div>
            </a>
        </div>
    )
}

export default CardCategory