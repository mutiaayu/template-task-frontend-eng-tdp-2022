import React from 'react'
import { useFetch } from 'hooks'
import CardProduct from 'components/CardProduct'
import { useSearchParams } from 'react-router-dom'

const Shop = () => {

    const [searchParams] = useSearchParams();
    const { data, isLoading } = useFetch(
        'https://api-tdp-2022.vercel.app/api/products',
        { category: searchParams.get('category') }
    )

    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className='breadcrumb'>
                    <span>
                        <a href='/'>Home</a>
                        <span className="breadcrumb-separator">/</span>
                    </span>
                    <span>
                        <a href="/">Shop</a>
                    </span>
                </div>
            </div>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">
                    All Product
                </h3>
            </div>
            {isLoading ?
                <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                    Loading . . .
                </div> :
                <div className="row-padding rm-before-after" style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
                    {
                        data?.data?.productList ? data?.data?.productList?.map((obj, i) => {
                            return (
                                <CardProduct
                                    key={i}
                                    id={obj.id}
                                    title={obj.title}
                                    image={obj.image}
                                    discount={obj.discount}
                                    description={obj.description}
                                    price={obj.price}
                                />
                            )
                        }) :
                            <div>Product Not Found</div>
                    }
                </div>
            }
        </div>
    )
}

export default Shop