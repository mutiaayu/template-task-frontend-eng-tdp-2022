import React, { useState } from 'react'
import { useFetch } from 'hooks'
import { useParams } from 'react-router';
import { useNavigate } from 'react-router-dom';

const ShopDetail = () => {
    const { id } = useParams();
    const { data: product, isLoading } = useFetch(
        `https://api-tdp-2022.vercel.app/api/products/${id}`
    )
    const { data } = { ...product };
    const navigate = useNavigate();
    const [qty, setQty] = useState(0);
    const discountPrice = data?.price - (data?.price * (data?.discount / 100));

    const handleChangeQty = (e) => {
        const qtyValue = e.target.value
        if (qtyValue < 0 || qtyValue > data?.stock) return
        return setQty(qtyValue)
    }

    const handleClickAdd = () => {
        alert('Product added to cart successfully');
        navigate('/shop');
    };
    return (
        <div className="content padding" style={{ maxWidth: '1564px' }}>
            <div className="container" style={{ marginTop: '80px' }}>
                <div className='breadcrumb'>
                    <span>
                        <a href='/'>Home</a>
                        <span className="breadcrumb-separator">/</span>
                    </span>
                    <span>
                        <a href="/">Shop</a>
                        <span className="breadcrumb-separator">/</span>
                        <a href="/">Detail</a>
                    </span>
                </div>
            </div>
            <div className="container padding-32" id="about">
                <h3 className="border-bottom border-light-grey padding-16">Product Information</h3>
            </div>
            {isLoading ?
                <div className="row-padding padding-large" style={{ fontSize: '16px', fontWeight: '800', textAlign: 'center' }}>
                    Loading . . .
                </div> :
                <div className="row-padding card card-shadow padding-large" style={{ marginTop: '50px' }}>
                    <div className="col l3 m6 margin-bottom">
                        <div className="product-tumb">
                            <img src={data?.image} alt="Product 1" />
                        </div>
                    </div>
                    <div className="col m6 margin-bottom">
                        <h3>{data?.title}</h3>
                        <div style={{ marginBottom: '32px' }}>
                            <span>Category : <strong>{data?.category}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Review : <strong>{data?.rate}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Stock : <strong>{data?.stock}</strong></span>
                            <span style={{ marginLeft: '30px' }}>Discount : <strong>{data?.discount} %</strong></span>
                        </div>
                        <div style={{ fontSize: '2rem', lineHeight: '34px', fontWeight: '800', marginBottom: '32px' }}>
                            Rp. {parseInt(data?.price).toLocaleString('id')}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            {data?.description}
                        </div>
                        <div style={{ marginBottom: '32px' }}>
                            <div><strong>Quantity : </strong></div>
                            <input type="number" className="input section border" name="total" placeholder='Quantity' min={0} max={data?.stock} value={qty} onChange={(e) => handleChangeQty(e)} />
                        </div>
                        <div style={{ marginBottom: '32px', fontSize: '2rem', fontWeight: 800 }}>
                            Sub Total : Rp. {(discountPrice * qty).toLocaleString('id')}
                            <span style={{ marginLeft: '30px', fontSize: '18px', textDecoration: 'line-through' }}><strong>Rp. {(parseInt(data?.price) * qty).toLocaleString('id')}</strong></span>
                        </div>
                        {data?.stock === 0 ?
                            <button className='button light-grey block' disabled={true}>Empty Stock</button> :
                            <button className='button light-grey block' onClick={handleClickAdd}>Add to cart</button>
                        }
                    </div>
                </div>
            }
        </div>
    )
}

export default ShopDetail