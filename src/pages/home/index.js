import Banner from "components/Banner";
import ProductCategory from "components/ProductCategory";

const Home = () => {
    return (
        <>
            <Banner/>
            <ProductCategory/>
        </>
    )
}

export default Home;
